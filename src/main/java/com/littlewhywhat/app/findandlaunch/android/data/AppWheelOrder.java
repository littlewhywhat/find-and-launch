package com.littlewhywhat.app.findandlaunch.android.data;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(primaryKeys = { "wheelId", "order" }, foreignKeys = @ForeignKey(entity = AppInfoEntity.class,
                                  parentColumns = "package_name",
                                  childColumns = "app_package_name"))
public class AppWheelOrder {
    @ColumnInfo(name = "wheelId")
    public int wheelId;
    @ColumnInfo(name = "order")
    public int order;

    @ColumnInfo(name = "app_package_name")
    public String appPackageName;

    @ColumnInfo(name = "folderWheelId")
    public Integer folderWheelId;

    @ColumnInfo(name = "folderIconId")
    public Integer folderIconId;
}
