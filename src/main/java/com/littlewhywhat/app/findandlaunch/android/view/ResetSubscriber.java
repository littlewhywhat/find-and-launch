package com.littlewhywhat.app.findandlaunch.android.view;

public interface ResetSubscriber {
    void onReset(ViewWheelAdapter adapter);
}
