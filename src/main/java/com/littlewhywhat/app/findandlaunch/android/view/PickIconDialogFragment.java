package com.littlewhywhat.app.findandlaunch.android.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.littlewhywhat.hello.android.R;

import java.util.HashMap;
import java.util.Map;

public class PickIconDialogFragment extends DialogFragment {
    private static final Map<String, Integer> ICONS = new HashMap<String, Integer>();
    private final PickIconDialogListener listener;
    private String[] iconIds;

    public interface PickIconDialogListener {
        void onIconSelected(Integer iconId);
    }

    PickIconDialogFragment(PickIconDialogListener listener) {
        super();
        this.listener = listener;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Resources res = getResources();
        iconIds = res.getStringArray(R.array.folder_icons_ids);
        ICONS.put(iconIds[0], R.drawable.ic_music);
        ICONS.put(iconIds[1], R.drawable.ic_build);
        ICONS.put(iconIds[2], R.drawable.ic_money);
        ICONS.put(iconIds[3], R.drawable.ic_message);
        ICONS.put(iconIds[4], R.drawable.ic_local_library);
        ICONS.put(iconIds[5], R.drawable.ic_map);
        ICONS.put(iconIds[6], R.drawable.ic_create);
        ICONS.put(iconIds[7], R.drawable.ic_access_alarm);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(R.array.folder_icons_ids, (dialog, id) -> {
            listener.onIconSelected(ICONS.get(iconIds[id]));
        });
        return builder.create();
    }
}
