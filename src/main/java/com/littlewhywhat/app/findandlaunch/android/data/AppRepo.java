package com.littlewhywhat.app.findandlaunch.android.data;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.Collection;
import java.util.List;

public class AppRepo {
    private AppWheelOrderDao appWheelOrderDao;
    private LiveData<List<AppWheelOrderDao.AppWheelOrderInfo>> allWheelOrders;

    // Note that in order to unit test the AppRepo, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    public AppRepo(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        appWheelOrderDao = db.appWheelOrderDao();
        allWheelOrders = appWheelOrderDao.getAllAppWheelOrders();
    }

    public LiveData<List<AppWheelOrderDao.AppWheelOrderInfo>> getAllWheelOrders() {
        return allWheelOrders;
    }

    public void insertAppInfos(Collection<AppInfoEntity> values) {
        AppDatabase.databaseWriteExecutor
                .execute(() -> appWheelOrderDao.insertAppInfos(values));
    }
    public void insertOrderInfos(List<AppWheelOrder> appWheelOrders) {
        AppDatabase.databaseWriteExecutor
                .execute(() -> appWheelOrderDao.insertOrderInfos(appWheelOrders));
    }
    public void updateAppWheelOrder(AppWheelOrder appWheelOrder) {
        AppDatabase.databaseWriteExecutor
                .execute(() -> appWheelOrderDao.updateAppWheelOrder(appWheelOrder));
    }

    public void createAppWheelOrder(AppWheelOrder appWheelOrder, AppInfoEntity appInfoEntity) {
        AppDatabase.databaseWriteExecutor
                .execute(() -> appWheelOrderDao.createAppWheelOrder(appWheelOrder, appInfoEntity));
    }

    public void createAppWheel(AppWheelOrder folderWheelOrder, List<AppWheelOrder> wheelAppOrders, List<AppInfoEntity> wheelAppInfos) {
        AppDatabase.databaseWriteExecutor
                .execute(() -> appWheelOrderDao.createAppWheel(folderWheelOrder, wheelAppOrders, wheelAppInfos));
    }
}
