package com.littlewhywhat.app.findandlaunch.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.littlewhywhat.app.findandlaunch.android.data.ActivityInfoRepo;
import com.littlewhywhat.hello.android.R;

public class AppLaunchView extends AppCompatImageView {
    private String packageName = "";
    private String activityName = "";
    private int order = 0;
    private Integer folderWheelId = null;
    private ActivityInfoRepo activityInfoRepo;

    public AppLaunchView(Context context) {
        super(context);
        init(null, 0);
    }

    public AppLaunchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public AppLaunchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attrs, int defStyle) {
        activityInfoRepo = new ActivityInfoRepo(getContext());
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.AppLaunchView, defStyle, 0);

        try {
            packageName = a.getString(R.styleable.AppLaunchView_packageName);
            activityName = a.getString(R.styleable.AppLaunchView_activityName);
        } finally {
            a.recycle();
        }
    }

    public AppLaunchView asFolder(Integer wheelId, Integer iconId) {
        this.folderWheelId = wheelId;
        setImageResource(iconId);
        setColorFilter(Color.GRAY);
        invalidate();
        return this;
    }

    public AppLaunchView withAppInfoOrder(AppInfo appInfo, int order) {
        this.order = order;
        this.folderWheelId = appInfo.folderWheelId;
        if (appInfo.folderWheelId != null) {
            setColorFilter(ViewWheelAdapter.COLORS[appInfo.folderWheelId % ViewWheelAdapter.COLORS.length]);
            if (appInfo.folderIconId != null) {
                setImageResource(appInfo.folderIconId);
            } else {
                setImageResource(R.drawable.ic_folder_open_black_24dp);
            }
            return this;
        }
        return withPackages(appInfo.packageName.toString(), appInfo.activityName);
    }

    public AppLaunchView withPackages(String packageName, String activityName) {
        this.packageName = packageName;
        this.activityName = activityName;
        if (this.folderWheelId == null) {
            setImageDrawable(activityInfoRepo.getIcon(packageName, activityName));
        }
        return this;
    }

    public int getPos() {
        return order;
    }

    public Integer getFolderWheelId() {
        return folderWheelId;
    }

    public String getPackageName() {
        return packageName;
    }

}