package com.littlewhywhat.app.findandlaunch.android.view;

import com.littlewhywhat.app.findandlaunch.android.viewmodel.AppListViewModel;
import com.littlewhywhat.app.findandlaunch.android.viewmodel.AppWheelOrderViewModel;
import com.littlewhywhat.hello.android.R;

import android.app.Application;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.DragEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity {
    private static final int REQUEST_PICK_APPWIDGET = 12312;
    private static final int REQUEST_CREATE_APPWIDGET = 12313;
    private ViewWheelAdapter adapter;
    private ConstraintLayout widgetSpace;

    private AppWidgetManager mAppWidgetManager;
    private AppWidgetHost mAppWidgetHost;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main);

        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 4);
        recyclerView.setLayoutManager(layoutManager);


        RAdapter mAdapter = new RAdapter(this);
        recyclerView.setAdapter(mAdapter);

        ViewWheel viewWheel = findViewById(R.id.viewWheel);

        final ViewModelProvider viewModelProvider = new ViewModelProvider(getViewModelStore(),
                new ViewModelProvider.AndroidViewModelFactory((Application) getApplicationContext()));
        AppWheelOrderViewModel appWheelOrderViewModel =
                viewModelProvider.get(AppWheelOrderViewModel.class);
        adapter = new ViewWheelAdapter(this, appWheelOrderViewModel);
        viewWheel.setAdapter(adapter);

        MotionLayout motionLayout = this.findViewById(R.id.motionLayout);
        motionLayout.setOnDragListener((view, dragEvent) -> {
            if (dragEvent.getAction() == DragEvent.ACTION_DRAG_STARTED) {
                motionLayout.transitionToState(R.id.base_state);
            }
            return false;
        });
        widgetSpace = findViewById(R.id.widget_space);
        final ImageView uninstallSpace = findViewById(R.id.uninstall_space);
        uninstallSpace.setOnDragListener((view, dragEvent) -> {
            final int action = dragEvent.getAction();
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    uninstallSpace.setImageResource(R.drawable.ic_zdelete_black_24dp);
                case DragEvent.ACTION_DRAG_EXITED:
                    uninstallSpace.setBackgroundColor(Color.BLUE);
                    uninstallSpace.invalidate();
                    return true;
                case DragEvent.ACTION_DRAG_ENTERED:
                    uninstallSpace.setBackgroundColor(Color.GREEN);
                    uninstallSpace.invalidate();
                    return true;
                case DragEvent.ACTION_DRAG_LOCATION:
                    return true;
                case DragEvent.ACTION_DROP:
                    ClipData.Item packageNameItem = dragEvent.getClipData().getItemAt(0);
                    String packageName = packageNameItem.getText().toString();
                    Log.i(MainActivity.class.getSimpleName(), "Uninstalling " + packageName);
                    Uri packageURI = Uri.parse("package:" + packageName);
                    Intent uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageURI);
                    startActivity(uninstallIntent);
                    return true;
                case DragEvent.ACTION_DRAG_ENDED:
                    uninstallSpace.setImageResource(0);
                    uninstallSpace.setBackgroundColor(Color.BLACK);
                    uninstallSpace.invalidate();
                    return true;
                default:
                    break;
            }
            return false;
        });
        findViewById(R.id.select_widget_button).setOnClickListener((event) -> {
            selectWidget();
        });
        mAppWidgetManager = AppWidgetManager.getInstance(this);
        mAppWidgetHost = new AppWidgetHost(this, 1025);
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        int widgetId = sharedPref.getInt(getString(R.string.widget_id), -1);
        if (widgetId != -1) {
            addWidgetById(widgetId);
        }
        AppListViewModel appListViewModel = new AppListViewModel(getApplication());
        appListViewModel.getAppList().observe(this, mAdapter::setData);
	}

	@Override
    protected void onStop() {
        super.onStop();
        mAppWidgetHost.stopListening();
    }

    void selectWidget() {
        int appWidgetId = this.mAppWidgetHost.allocateAppWidgetId();
        Intent pickIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_PICK);
        pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        addEmptyData(pickIntent);
        startActivityForResult(pickIntent, REQUEST_PICK_APPWIDGET);
    }
    void addEmptyData(Intent pickIntent) {
        ArrayList customInfo = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_INFO, customInfo);
        ArrayList customExtras = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_EXTRAS, customExtras);
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK ) {
            if (requestCode == REQUEST_PICK_APPWIDGET) {
                configureWidget(data);
            }
            else if (requestCode == REQUEST_CREATE_APPWIDGET) {
                createWidget(data);
            }
        }
        else if (resultCode == RESULT_CANCELED && data != null) {
            int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
            if (appWidgetId != -1) {
                mAppWidgetHost.deleteAppWidgetId(appWidgetId);
            }
        }
    }

    private void configureWidget(Intent data) {
        Bundle extras = data.getExtras();
        int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo(appWidgetId);
        if (appWidgetInfo.configure != null) {
            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            intent.setComponent(appWidgetInfo.configure);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            startActivityForResult(intent, REQUEST_CREATE_APPWIDGET);
        } else {
            createWidget(data);
        }
    }

    public void createWidget(Intent data) {
        Bundle extras = data.getExtras();
        int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.widget_id), appWidgetId);
        editor.apply();
        addWidgetById(appWidgetId);
    }

    private void addWidgetById(int appWidgetId) {
        AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo(appWidgetId);
        AppWidgetHostView hostView = mAppWidgetHost.createView(this, appWidgetId, appWidgetInfo);
        hostView.setAppWidget(appWidgetId, appWidgetInfo);
        hostView.setLayoutParams(
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
        widgetSpace.removeAllViews();
        widgetSpace.addView(hostView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAppWidgetHost.startListening();
    }

    public void removeWidget(AppWidgetHostView hostView) {
        mAppWidgetHost.deleteAppWidgetId(hostView.getAppWidgetId());
        widgetSpace.removeView(hostView);
    }
}