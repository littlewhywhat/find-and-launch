package com.littlewhywhat.app.findandlaunch.android.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.littlewhywhat.app.findandlaunch.android.data.AppInfoEntity;
import com.littlewhywhat.app.findandlaunch.android.data.AppRepo;
import com.littlewhywhat.app.findandlaunch.android.data.AppWheelOrder;
import com.littlewhywhat.app.findandlaunch.android.data.AppWheelOrderDao;

import java.util.List;

public class AppWheelOrderViewModel extends AndroidViewModel {
    private final AppRepo repository;
    private final LiveData<List<AppWheelOrderDao.AppWheelOrderInfo>> allWheelOrders;

    public AppWheelOrderViewModel(Application application) {
        super(application);
        repository = new AppRepo(application);
        allWheelOrders = repository.getAllWheelOrders();
    }

    public LiveData<List<AppWheelOrderDao.AppWheelOrderInfo>> getAllWheelOrders() {
        return allWheelOrders;
    }
    public void updateAppWheelOrder(AppWheelOrder appWheelOrder) {
        repository.updateAppWheelOrder(appWheelOrder);
    }

    public void createAppWheelOrder(AppWheelOrder appWheelOrder, AppInfoEntity appInfoEntity) {
        repository.createAppWheelOrder(appWheelOrder, appInfoEntity);
    }

    public void createAppWheel(AppWheelOrder folderWheelOrder, List<AppWheelOrder> wheelAppOrders, List<AppInfoEntity> wheelAppInfos) {
        repository.createAppWheel(folderWheelOrder, wheelAppOrders, wheelAppInfos);
    }
}
