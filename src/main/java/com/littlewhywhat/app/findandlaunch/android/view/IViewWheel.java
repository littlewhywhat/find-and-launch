package com.littlewhywhat.app.findandlaunch.android.view;

public interface IViewWheel {

    void finishToRotate(float degrees, float endDegrees);

    void reset();
}
