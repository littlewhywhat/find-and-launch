package com.littlewhywhat.app.findandlaunch.android.presenter;

import android.view.MotionEvent;

import com.littlewhywhat.app.findandlaunch.android.utils.MathUtils;
import com.littlewhywhat.app.findandlaunch.android.view.IViewWheel;

import java.util.ArrayList;
import java.util.List;

public class ViewWheelPresenter {
    private static final float CLOSE_RANGE = 4f;

    private float startTouchX;
    private float startTouchY;
    private int centerX;
    private int centerY;
    private boolean isClick;
    private IViewWheel viewWheel;
    private final List<RotatableViewPresenter> rotatableViewPresenters;
    private RotatableViewPresenter centerViewPresenter;

    private float degrees = 0;

    public ViewWheelPresenter() {
        rotatableViewPresenters = new ArrayList<>();
    }

    public void presentTo(IViewWheel viewWheel) {
        this.viewWheel = viewWheel;
    }

    public void onLayout(int left, int top, int right, int bottom) {
        final int width = right - left;
        final int height = bottom - top;
        final int newTop = width > height? 0 : Math.abs(height - width) / 2;
        centerY = height / 2;
        centerX = width / 2;
        final int radius = width > height? centerY : centerX;

        float rotation = degrees;
        for (final RotatableViewPresenter rotatableViewPresenter : rotatableViewPresenters) {
            rotation -= 360f/ rotatableViewPresenters.size();
            rotatableViewPresenter.layout(centerX, newTop, radius, rotation);
        }
        if (centerViewPresenter != null) {
            centerViewPresenter.layout(centerX, centerY - 75, radius, 0);
        }
    }

    public void addRotatableViewPresenter(RotatableViewPresenter presenter) {
        rotatableViewPresenters.add(presenter);
    }

    public void setCenterViewPresenter(RotatableViewPresenter presenter) {
        centerViewPresenter = presenter;
    }

    public void setDegrees(float degrees) {
        this.degrees = degrees;
        viewWheel.reset();
    }

    public void rotate(MotionEvent motion) {
        switch (motion.getAction()) {
            case MotionEvent.ACTION_MOVE:
                continueToRotate(motion);
                break;
            case MotionEvent.ACTION_UP:
                finishToRotate(motion);
                break;
        }
    }

    public boolean isClick(MotionEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouchX = touchEvent.getX();
                startTouchY = touchEvent.getY();
                isClick = true;
                break;
            case MotionEvent.ACTION_MOVE:
                if (!MathUtils.pointsAreClose(startTouchX, startTouchY,
                        touchEvent.getX(), touchEvent.getY(), CLOSE_RANGE)) {
                    isClick = false;
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                isClick = false;
        }
        return isClick;
    }

    public void reset() {
        degrees = 0;
        rotatableViewPresenters.clear();
        centerViewPresenter = null;
    }

    private void continueToRotate(MotionEvent motionEvent) {
        if (motionEvent.getHistorySize() > 0) {
            degrees += (float) MathUtils.computeAngle(centerX, centerY,
                    motionEvent.getX(), motionEvent.getY(),
                    motionEvent.getHistoricalX(0), motionEvent.getHistoricalY(0));
            viewWheel.reset();
        }
    }

    private void finishToRotate(MotionEvent motionEvent) {
        continueToRotate(motionEvent);
        viewWheel.finishToRotate(degrees, getEndDegrees());
    }

    private float getEndDegrees() {
        final float degreesInSegment = (float)(360 / rotatableViewPresenters.size());
        return Math.round(degrees / degreesInSegment) * degreesInSegment;
    }
}
