package com.littlewhywhat.app.findandlaunch.android.view;

import android.graphics.drawable.Drawable;

public class AppInfo {
    Integer folderIconId;
    int wheelId;
    Integer folderWheelId;
    public CharSequence label;
    public CharSequence packageName;
    public Drawable icon;
    public String activityName;
}
