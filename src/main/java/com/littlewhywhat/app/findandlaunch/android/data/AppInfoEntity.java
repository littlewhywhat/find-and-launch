package com.littlewhywhat.app.findandlaunch.android.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class AppInfoEntity {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "package_name")
    public String packageName;

    @ColumnInfo(name = "activity_name")
    public String activityName;
}
