package com.littlewhywhat.app.findandlaunch.android.view;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.littlewhywhat.app.findandlaunch.android.presenter.RotatableViewPresenter;
import com.littlewhywhat.app.findandlaunch.android.presenter.ViewWheelPresenter;

class ViewWheel extends ViewGroup implements IViewWheel, ResetSubscriber {
    private ViewWheelPresenter presenter;

    public ViewWheel(Context context) {
        super(context);
        init();
    }

    public ViewWheel(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ViewWheel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        presenter = new ViewWheelPresenter();
        presenter.presentTo(this);
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent touchEvent) {
        return !presenter.isClick(touchEvent);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(android.view.MotionEvent motion) {
        presenter.rotate(motion);
        return true;
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            getChildAt(i).requestLayout();
        }
    }

    @Override
    public void finishToRotate(float from, float to) {
        ValueAnimator animator = ValueAnimator.ofFloat(from, to);
        animator.addUpdateListener(valueAnimator ->
                presenter.setDegrees((float)valueAnimator.getAnimatedValue()));
        animator.setInterpolator(new DecelerateInterpolator());
        animator.start();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.i("layout", "onLayout");
        presenter.onLayout(left, top, right, bottom);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            measureChild(getChildAt(i), widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public void addView(View view) {
        final RotatableViewPresenter childPresenter = new RotatableViewPresenter().presentTo(view);
        childPresenter.setSize(150, 150);
        presenter.addRotatableViewPresenter(childPresenter);
        super.addView(view);
    }

    @Override
    public void reset() {
        invalidate();
        requestLayout();
    }

    public void setAdapter(ViewWheelAdapter viewWheelAdapter) {
        for (int i = 0; i < viewWheelAdapter.getItemCount(); i++) {
            addView(viewWheelAdapter.getView(i));
        }
        viewWheelAdapter.subscribe(this);
    }

    @Override
    public void onReset(ViewWheelAdapter viewWheelAdapter) {
        removeAllViews();
        presenter.reset();
        for (int i = 0; i < viewWheelAdapter.getItemCount(); i++) {
            addView(viewWheelAdapter.getView(i));
        }
        View centerView = viewWheelAdapter.getCenterView();
        if (centerView != null) {
            final RotatableViewPresenter centerPresenter = new RotatableViewPresenter().presentTo(centerView);
            centerPresenter.setSize(150, 150);
            presenter.setCenterViewPresenter(centerPresenter);
            super.addView(centerView);
        }
    }
}
