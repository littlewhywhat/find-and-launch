package com.littlewhywhat.app.findandlaunch.android.view;

import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.littlewhywhat.app.findandlaunch.android.data.AppInfoEntity;
import com.littlewhywhat.app.findandlaunch.android.data.AppWheelOrder;
import com.littlewhywhat.app.findandlaunch.android.data.AppWheelOrderDao;
import com.littlewhywhat.app.findandlaunch.android.viewmodel.AppWheelOrderViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class ViewWheelAdapter implements View.OnDragListener, View.OnLongClickListener, View.OnClickListener, PickIconDialogFragment.PickIconDialogListener {
    static final int[] COLORS = {
        Color.RED, Color.CYAN, Color.MAGENTA, Color.YELLOW, Color.GREEN
    };
    private static final int APP_CNT = 8;
    private final FragmentActivity context;
    private final AppWheelOrderViewModel appWheelOrderViewModel;
    private ResetSubscriber subscriber;
    private Map<Integer, Map<Integer, AppInfo>> appsByWheelId = new HashMap<>();
    private Integer currentWheelId = 0;
    private int maxWheelId = 0;
    private List<ResolveInfo> resolveInfos = new ArrayList<>();
    private Stack<Integer> wheelStack = new Stack<>();
    private AppLaunchView editView;

    ViewWheelAdapter(FragmentActivity context, AppWheelOrderViewModel appWheelOrderViewModel) {
        this.context = context;
        this.appWheelOrderViewModel = appWheelOrderViewModel;

        appWheelOrderViewModel.getAllWheelOrders().observe(context, (orders) -> {
            Log.d(ViewWheelAdapter.class.getSimpleName(), "Loading:");
            reset();
            appsByWheelId.put(currentWheelId, new HashMap<>());
            for (AppWheelOrderDao.AppWheelOrderInfo order : orders) {
                final AppInfo appInfo = new AppInfo();
                appInfo.activityName = order.activityName;
                appInfo.packageName = order.packageName;
                appInfo.wheelId = order.wheelId;
                appInfo.folderWheelId = order.folderWheelId;
                appInfo.folderIconId = order.folderIconId;
                if (!appsByWheelId.containsKey(order.wheelId)) {
                    appsByWheelId.put(order.wheelId, new HashMap<>());
                }
                if (order.folderWheelId != null && !appsByWheelId.containsKey(order.folderWheelId)) {
                    appsByWheelId.put(order.folderWheelId, new HashMap<>());
                }
                appsByWheelId.get(order.wheelId).put(order.appOrder, appInfo);
                if (order.wheelId > maxWheelId) {
                    maxWheelId = order.wheelId;
                }

                Log.d(ViewWheelAdapter.class.getSimpleName(),
                        String.format("|%d|%10s|%10s|%d|",
                                order.appOrder,
                                order.activityName,
                                order.packageName,
                                order.folderWheelId));
            }

            PackageManager pm = context.getPackageManager();

            Intent intent = new Intent(Intent.ACTION_MAIN, null);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> allApps = pm.queryIntentActivities(intent, 0);

            for (int i = 0; i < APP_CNT; i++) {
                resolveInfos.add(allApps.get(i));
            }

            for (Map<Integer, AppInfo> wheel : appsByWheelId.values()) {
                for(int i = 0; i < APP_CNT; i++) {
                    if (!wheel.containsKey(i)) {
                        ResolveInfo ri = allApps.get(i);
                        final AppInfo appInfo = new AppInfo();
                        appInfo.wheelId = i;
                        appInfo.folderWheelId = null;
                        appInfo.activityName = ri.activityInfo.name;
                        appInfo.packageName = ri.activityInfo.packageName;

                        wheel.put(i, appInfo);
                    }
                }
            }
            if (subscriber != null) {
                subscriber.onReset(this);
            }
        });
    }

    void subscribe(ResetSubscriber subscriber) {
        this.subscriber = subscriber;
    }

    int getItemCount() {
        Map<Integer, AppInfo> appWheel = appsByWheelId.get(currentWheelId);
        if (appWheel == null) {
            return 0;
        }
        return appWheel.size();
    }

    View getView(int pos) {
        AppInfo appInfo = appsByWheelId.get(currentWheelId).get(pos);
        final GyroContainer gyro = new GyroContainer(context);
        final AppLaunchView appView = new AppLaunchView(context).withAppInfoOrder(
                appInfo, pos
        );
        appView.setOnClickListener(this);
        appView.setOnLongClickListener(this);
        appView.setOnDragListener(this);
        gyro.addView(appView);
        return gyro;
    }

    View getCenterView() {
        if (wheelStack.empty()) {
            return null;
        }
        ResolveInfo ri = resolveInfos.get(0);
        final AppInfo appInfo = new AppInfo();
        appInfo.folderWheelId = currentWheelId;
        appInfo.activityName = ri.activityInfo.name;
        appInfo.packageName = ri.activityInfo.packageName;
        final AppLaunchView appView = new AppLaunchView(context).withAppInfoOrder(
                appInfo, 0
        );
        ViewWheelAdapter adapter = this;
        appView.setOnClickListener(view -> {
            currentWheelId = adapter.wheelStack.pop();
            subscriber.onReset(adapter);
        });
        return appView;
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        final AppLaunchView appLaunchView = (AppLaunchView)view;
        final int action = dragEvent.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
            case DragEvent.ACTION_DRAG_EXITED:
                appLaunchView.setColorFilter(Color.BLUE);
                appLaunchView.invalidate();
                return true;
            case DragEvent.ACTION_DRAG_ENTERED:
                appLaunchView.setColorFilter(Color.GREEN);
                appLaunchView.invalidate();
                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                return true;
            case DragEvent.ACTION_DROP:
                ClipData.Item packageNameItem = dragEvent.getClipData().getItemAt(0);
                CharSequence packageName = packageNameItem.getText();
                ClipData.Item activityNameItem = dragEvent.getClipData().getItemAt(1);
                CharSequence activityName = activityNameItem.getText();

                AppInfo appInfo = appsByWheelId.get(currentWheelId).get(appLaunchView.getPos());
                final AppWheelOrder appWheelOrder = new AppWheelOrder();
                appWheelOrder.wheelId = appInfo.wheelId;
                appWheelOrder.appPackageName = packageName.toString();
                appWheelOrder.folderWheelId = null;
                appWheelOrder.folderIconId = null;
                appWheelOrder.order = appLaunchView.getPos();
                AppInfoEntity appInfoEntity = new AppInfoEntity();
                appInfoEntity.activityName = activityName.toString();
                appInfoEntity.packageName = packageName.toString();
                appWheelOrderViewModel.createAppWheelOrder(appWheelOrder, appInfoEntity);

                appLaunchView.clearColorFilter();
                appLaunchView.invalidate();
                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                if (appLaunchView.getFolderWheelId() == null) {
                    appLaunchView.clearColorFilter();
                } else {
                    appLaunchView.setColorFilter(COLORS[appLaunchView.getFolderWheelId() % COLORS.length]);
                }
                appLaunchView.invalidate();
                return true;

            default:
                break;
        }
        return false;
    }

    @Override
    public boolean onLongClick(View view) {
        final AppLaunchView appLaunchView = (AppLaunchView)view;
        editView = appLaunchView;
        PickIconDialogFragment pickIconDialog = new PickIconDialogFragment(this);
        pickIconDialog.show(context.getSupportFragmentManager(), "pick_folder_icon");
        return true;
    }

    @Override
    public void onClick(View view) {
        final AppLaunchView appLaunchView = (AppLaunchView)view;
        if (appLaunchView.getFolderWheelId() == null) {
            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(appLaunchView.getPackageName());
            context.startActivity(launchIntent);
        } else {
            // switch to folder id
            wheelStack.push(currentWheelId);
            currentWheelId = appLaunchView.getFolderWheelId();
            subscriber.onReset(this);
        }
    }

    @Override
    public void onIconSelected(Integer iconId) {
        if (editView.getFolderWheelId() == null) {
            AppWheelOrder folderWheelOrder = new AppWheelOrder();
            folderWheelOrder.order = editView.getPos();
            folderWheelOrder.folderIconId = iconId;
            folderWheelOrder.folderWheelId = maxWheelId + 1;
            folderWheelOrder.wheelId = currentWheelId;
            List<AppWheelOrder> wheelAppOrders = new ArrayList<>();
            List<AppInfoEntity> wheelAppInfos = new ArrayList<>();
            for(int i = 0; i < APP_CNT; i++) {
                ResolveInfo ri = resolveInfos.get(i);
                final AppWheelOrder appWheelOrder = new AppWheelOrder();
                appWheelOrder.wheelId = folderWheelOrder.folderWheelId;
                appWheelOrder.order = i;
                appWheelOrder.appPackageName = ri.activityInfo.packageName;
                AppInfoEntity appInfoEntity = new AppInfoEntity();
                appInfoEntity.activityName = ri.activityInfo.name;
                appInfoEntity.packageName = ri.activityInfo.packageName;
                wheelAppOrders.add(appWheelOrder);
                wheelAppInfos.add(appInfoEntity);
            }
            appWheelOrderViewModel.createAppWheel(folderWheelOrder, wheelAppOrders, wheelAppInfos);
        } else {
            AppInfo appInfo = appsByWheelId.get(currentWheelId).get(editView.getPos());
            final AppWheelOrder appWheelOrder = new AppWheelOrder();
            appWheelOrder.wheelId = appInfo.wheelId;
            appWheelOrder.folderWheelId = appInfo.folderWheelId;
            appWheelOrder.order = editView.getPos();
            appWheelOrder.folderIconId = iconId;
            appWheelOrderViewModel.updateAppWheelOrder(appWheelOrder);
        }
    }

    private void reset() {
        appsByWheelId.clear();
        maxWheelId = 0;
        resolveInfos.clear();
    }
}
