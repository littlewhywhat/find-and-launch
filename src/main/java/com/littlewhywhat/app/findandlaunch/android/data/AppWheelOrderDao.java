package com.littlewhywhat.app.findandlaunch.android.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Collection;
import java.util.List;

@Dao
public abstract class AppWheelOrderDao {
    @Query("SELECT AppWheelOrder.wheelId AS wheelId, AppWheelOrder.`order` AS appOrder," +
            " AppWheelOrder.folderWheelId AS folderWheelId," +
            " AppInfoEntity.activity_name AS activityName, AppInfoEntity.package_name AS packageName," +
            " AppWheelOrder.folderIconId AS folderIconId" +
            " FROM AppWheelOrder " +
            "LEFT JOIN AppInfoEntity ON AppInfoEntity.package_name = AppWheelOrder.app_package_name")
    abstract LiveData<List<AppWheelOrderInfo>> getAllAppWheelOrders();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void insertOrderInfos(List<AppWheelOrder> infos);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void insertAppInfos(Collection<AppInfoEntity> values);

    @Update()
    abstract void updateAppWheelOrder(AppWheelOrder appWheelOrder);

    @Transaction
    void createAppWheelOrder(AppWheelOrder appWheelOrder, AppInfoEntity appInfoEntity) {
        insertAppInfo(appInfoEntity);
        updateAppWheelOrder(appWheelOrder);
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void insertAppInfo(AppInfoEntity appInfoEntity);

    @Transaction
    public void createAppWheel(AppWheelOrder folderWheelOrder,
                                        List<AppWheelOrder> wheelAppOrders,
                                        List<AppInfoEntity> wheelAppInfos) {
        updateAppWheelOrder(folderWheelOrder);
        insertAppInfos(wheelAppInfos);
        insertOrderInfos(wheelAppOrders);
    }

    public static class AppWheelOrderInfo {
        public int wheelId;
        public int appOrder;
        public String activityName;
        public String packageName;
        public Integer folderWheelId;
        public Integer folderIconId;
    }
}
