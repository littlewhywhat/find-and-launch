package com.littlewhywhat.app.findandlaunch.android.utils;

public class MathUtils {
    public static double computeAngle(float cx, float cy, float x1, float y1, float x2, float y2) {
        return Math.toDegrees(Math.atan2(y1 - cy, x1 - cx) - Math.atan2(y2 - cy, x2 - cx));
    }

    public static boolean pointsAreClose(float oneX, float oneY, float twoX, float twoY,
                                         float precision) {
        return (Math.abs(oneX - twoX) <= precision) && (Math.abs(oneY - twoY) <= precision);
    }
}
