package com.littlewhywhat.app.findandlaunch.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class GyroContainer extends ViewGroup {
    public GyroContainer(Context context) {
        super(context);
    }

    public GyroContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GyroContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (getChildCount() > 0) {
            final View child = getChildAt(0);
            final int width = right - left;
            final int height = bottom - top;
            child.setPivotX(width >> 1);
            child.setPivotY(height >> 1);
            child.setRotation(-getRotation());
            child.layout(0, 0, width, height);
        }
    }
}
