package com.littlewhywhat.app.findandlaunch.android.presenter;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

public class RotatableViewPresenter {
    private static final String TAG = RotatableViewPresenter.class.getSimpleName();

    private View rotatableView;

    public RotatableViewPresenter presentTo(View view) {
        rotatableView = view;
        return this;
    }

    public void setSize(int width, int height) {
        rotatableView.setLayoutParams(new LinearLayout.LayoutParams(width, height));
    }

    void layout(int x, int y, int radius, float rotation) {
        if (rotatableView != null) {
            final int childWidth = rotatableView.getMeasuredWidth();
            final int childHeight = rotatableView.getMeasuredHeight();
            final int halfChildWidth = childWidth / 2;

            // pivot points are relative to child layout
            rotatableView.setPivotX(halfChildWidth);
            rotatableView.setPivotY(radius);
            rotatableView.setRotation(rotation);
            rotatableView.layout(x - halfChildWidth, y,
                    x + halfChildWidth, y + childHeight);
        } else {
            Log.w(TAG, "No rotatable view to present to!");
        }
    }
}
