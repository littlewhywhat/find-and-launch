package com.littlewhywhat.app.findandlaunch.android.presenter;

import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static android.view.MotionEvent.ACTION_MOVE;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import static com.google.common.truth.Truth.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ButtonWheelTest {
    @Mock
    private View view;

    @Before
    public void setUp() {
    }

    @Test
    public void test1() {
        assertThat(ACTION_MOVE).isEqualTo(0);
        verify(view, never()).animate();
    }
}