const s = Snap('#main');

const calculateWheelBBox = () => {
  let wheelBBox = { x1: 0, y1: 0, x2: 0, y2: 0};
  
  const bodyWidth = $('body').width();
  const bodyHeight = $('body').height();
  
  if (bodyHeight > 0 && bodyWidth > 0) {
    const isLandscape = bodyWidth > bodyHeight;
    if (isLandscape) {
      wheelBBox.x1 = (bodyWidth - bodyHeight) / 2;
      wheelBBox.x2 = wheelBBox.x1 + bodyHeight;
      wheelBBox.y2 = bodyHeight;
    } else {
      wheelBBox.y1 = (bodyHeight - bodyWidth) / 2;
      wheelBBox.y2 = wheelBBox.y1 + bodyWidth;
      wheelBBox.x2 = bodyWidth;
    }
  }
  return wheelBBox;
}

let wheel = new AppWheel(s, calculateWheelBBox(), 6);

$(window).resize(() => {
  wheel.draw(calculateWheelBBox());
});

let rotateTouch = null;

$('#main').on('touchstart', function(event) {
  rotateTouch = new RotateTouch(s.getBBox(), event.touches[0]);
});

$('#main').on('touchmove', function(event) {
  wheel.rotate(rotateTouch.update(event.touches[0]).getCurrentDegrees());
});

$('#main').on('touchend', function(event) {
  const sectorsPassed = rotateTouch.getDegrees() / wheel.getDegree();
  const unfullSectorsPassed = sectorsPassed - Math.floor(sectorsPassed);
  if (unfullSectorsPassed < 0.5) {
    wheel.rotateBounce(-unfullSectorsPassed * wheel.getDegree());
  } else {
    wheel.rotateBounce((1 - unfullSectorsPassed) * wheel.getDegree());
  }
});
