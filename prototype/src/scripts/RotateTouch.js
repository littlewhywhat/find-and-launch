const DEGREE_TO_PIXELS_SCALE = 3;

class RotateTouch {
  constructor(center, touch) {
    this._center = center;
    this._startTouch = touch;
    this._currentTouch = touch;
    this._prevTouch = null;
    this._degrees = 0;
  }
  
  _touchIsCurrentlyVertical = () => {
    return Math.abs(this._currentTouch.clientY - this._prevTouch.clientY)
      > Math.abs(this._currentTouch.clientX - this._prevTouch.clientX);
  }
  
  _touchOnTop = () => {
    return this._prevTouch.clientY < this._center.cy;
  }
  
  _touchOnLeftSide = () => {
    return this._prevTouch.clientX < this._center.cx;
  }
  
  update = (touchUpdate) => {
    this._prevTouch = this._currentTouch;
    this._currentTouch = touchUpdate;
    this._degrees = this._degrees + this.getCurrentDegrees();
    return this;
  }
  
  getCurrentDegrees = () => {
    let pixels = 0;
    if (this._touchIsCurrentlyVertical()) {
      if (this._touchOnLeftSide()) {
        pixels = -(this._currentTouch.clientY - this._prevTouch.clientY);
      } else {
        pixels = (this._currentTouch.clientY - this._prevTouch.clientY)
      }
    } else {
      if (this._touchOnTop()) {
        pixels = -(this._prevTouch.clientX - this._currentTouch.clientX);
      } else {
        pixels = (this._prevTouch.clientX - this._currentTouch.clientX);
      }
    }
    return pixels/DEGREE_TO_PIXELS_SCALE;
  }
  
  getDegrees = () => {
    return this._degrees;
  }
}