const STROKE_WIDTH = 5;
const APP_RADIUS = 40;

class AppWheel {
  draw = (bbox) => {
    // draw a main circle with border in the center
    // radius is quarter of bigger dimension
    let radius = (bbox.x2 - bbox.x1) / 2;
    let center = {
      cx: bbox.x1 + radius,
      cy: bbox.y1 + radius
    };
    if (radius > (STROKE_WIDTH / 2 + APP_RADIUS)) {
      radius = radius - (STROKE_WIDTH / 2) - APP_RADIUS;
    }
    
    this._mainCircle.attr({
      cx: center.cx,
      cy: center.cy,
      r: radius,
      fill: 'white',
      stroke: 'black',
      strokeWidth: STROKE_WIDTH
    });
    // draw even n circles on the main circle
    let rotation = 0;
    this._appCircles.forEach((circle) => {
      circle.attr({
        cx: center.cx,
        cy: bbox.y1 + APP_RADIUS + STROKE_WIDTH,
        r: APP_RADIUS,
        fill: 'white',
        stroke: 'black',
        strokeWidth: STROKE_WIDTH,
      });
      rotation = rotation + this._appCircleDegree;
      circle.transform('r' + rotation + ',' + center.cx + ',' + center.cy);
    });
    
  }
  
  constructor(s, bbox, appsCnt) {
    this._s = s;
    this._appsCnt = appsCnt;
    this._mainCircle = this._s.circle(0, 0, 0);
    this._group = this._s.g();
    this._groupRotation = 0;
    this._appCircles = [];
    [...Array(appsCnt).keys()].forEach(() => {
      const circle = this._s.circle(0,0,0);
      this._appCircles.push(circle);
      this._group.add(circle);
    });
    this._appCircleDegree = 360/appsCnt;
    this.draw(bbox);
  }

  rotate = (degree) => {
    this._groupRotation = this._groupRotation + degree;
    const bbox = this._mainCircle.getBBox();
    this._group.transform('r' + this._groupRotation + ',' + bbox.cx + ',' + bbox.cy);
  }
  
  rotateBounce = (degree) => {
    this._groupRotation = this._groupRotation + degree;
    const bbox = this._mainCircle.getBBox();
    this._group.animate({
      transform: 'r' + this._groupRotation + ',' + bbox.cx + ',' + bbox.cy
    }, 500, mina.bounce);
  }
  
  getDegree = () => this._appCircleDegree;
}